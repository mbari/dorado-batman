/****************************************************************************/
/* Copyright (c) 2020 MBARI                                                 */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
/* Summary  :                                                               */
/* Filename : bl_shared.h                                                   */
/* Author   : etrauschke                                                    */
/* Project  :                                                               */
/* Version  : 1.0                                                           */
/* Created  : Jul 2, 2018                                                   */
/* Modified :                                                               */
/* Archived :                                                               */
/****************************************************************************/
/* Modification History:                                                    */
/****************************************************************************/

#ifndef BL_SHARED_H_
#define BL_SHARED_H_

#define BL_SHARED_ADDRESS	0x10000000
#define	BL_SHARED_MEM_MAGIC		("BOLO")

typedef struct bl_struct {
	char magic[4];
	char version[12];
	char reserved[48];
} bl_struct_t;

bl_struct_t *bl_shared = (bl_struct_t *)BL_SHARED_ADDRESS;

#endif /* BL_SHARED_H_ */
