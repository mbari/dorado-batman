/****************************************************************************/
/* Copyright (c) 2020 MBARI                                                 */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
/* Summary  :                                                               */
/* Filename : srv_ui.h                                                      */
/* Author   :                                                               */
/* Project  :                                                               */
/* Version  : 1.0                                                           */
/* Created  : 2019                                                          */
/* Modified :                                                               */
/* Archived :                                                               */
/****************************************************************************/
/* Modification History:                                                    */
/****************************************************************************/

#include "app_cfg.h"

int parse_ip_octet(char *param, char **ipstr);
int parse_ctl_cmd(APP_CFG *app, int sockidx, int len, void *client);
int parse_battery_cmd(APP_CFG *app, int sockidx, int len, void *client);

