/****************************************************************************/
/* Copyright (c) 2020 MBARI                                                 */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
/* Summary  :                                                               */
/* Filename : smbus.h                                                      */
/* Author   :                                                               */
/* Project  :                                                               */
/* Version  : 1.0                                                           */
/* Created  : 2016                                                          */
/* Modified :                                                               */
/* Archived :                                                               */
/****************************************************************************/
/* Modification History:                                                    */
/****************************************************************************/

#include "board.h"
#include "udp_types.h"

#define	VOLT_FULL	33.0
#define VOLT_EMPTY	28.0

/*i2c bus stuff*/
#define I2C_POLLING (1)
#define I2C_INTERRUPT (0)

/*smbus battery application specific stuff*/
#define INSTR_PWR_ENABLE_PIN   (4)
#define CHG_ENABLE_PIN   (5)
#define DISCH_ENABLE_PIN (6)
#define AUX_ENABLE_PIN   (7)
#define SMBUS_RESET_PIN  (8)
#define AUX2_ENABLE_PIN  (9)

#define	BATTYPE_UNKNOWN	0 
#define	BATTYPE_29		1
#define	BATTYPE_34		2

/* Battery smbus address */
#define BATTERY_ADDR	(0x16>>1)

/* charge WD timeout in sec */
#define CHGWD_TIMEOUT	120

/** Max buffer length */
#define BUFFER_SIZE			0x10
typedef enum {
	BATT_DISCH,
	BATT_OFF,
	BATT_CHG 
} BATTERY_STATE;

typedef struct {
	uint16_t serialNo;
	uint16_t status;
	uint16_t cellVolts[8];
	uint16_t stackVolt;
	uint16_t capacity_mAh;
	int16_t current;
	uint16_t cycleCnt;
	uint8_t state;
	uint8_t type;
	uint8_t flags;
	uint16_t temperature_Kx10;
} BATTERY_DATA;

#define PACK_ON		1<<0
#define PACK_OFF	1<<1
#define PACK_SHTDN_REQ	1<<2

//typedef struct {
//	uint16_t cell1;
//	uint16_t cell2;
//	uint16_t cell3;
//	uint16_t cell4;
//	uint16_t cell5;
//	uint16_t cell6;
//	uint16_t cell7;
//	uint16_t cell8;
//} CELL_VOLTAGES;

typedef enum {
	LED_PULSE_LEVEL,
	LED_SOLID_LEVEL,
	LED_BLINK
} LED_STATE;



void init_batteries(void);
void update_battery_status(void);
void update_batt_bin_struct(void);
void smbus_setup(void);
void allPacks(uint8_t state);
void chgwd_check(void);
void chgwd_control(bool enabled);
void chgwd_reset(void);
void set_led_state(LED_STATE user_led_state);

uint32_t get_smbus_errors(void);
uint32_t get_unrec_smbus_errors(void);
uint16_t get_battery_temp(void);
uint32_t get_humidity_temp(void);
uint16_t get_humidity(void);
uint16_t* get_voltages(void);
uint8_t get_bat_count(void);
int get_charge_level(void);
//BALL_DATA_884_T *get_cell_voltages(int batnum);
BATTERY_STATE get_batt_state(void);
void shutdown_packs(void);
