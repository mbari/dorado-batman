/****************************************************************************/
/* Copyright (c) 2020 MBARI                                                 */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
/* Summary  :                                                               */
/* Filename : uart.h                                                        */
/* Author   :                                                               */
/* Project  :                                                               */
/* Version  : 1.0                                                           */
/* Created  : 2019                                                          */
/* Modified :                                                               */
/* Archived :                                                               */
/****************************************************************************/
/* Modification History:                                                    */
/****************************************************************************/

#include "FreeRTOS.h"
#include "task.h"
#include "FreeRTOS_IP.h"
#include "app_cfg.h"

/* Transmit and receive ring buffer sizes (must be powers of 2) */
#define UART_SRB_SIZE 512	/* Send */
#define UART_RRB_SIZE 2048	/* Receive */

#define UART0_PORT		0
#define UART0_TX_PIN	2
#define UART0_RX_PIN	3

#if defined(X41CELL)
	#define UART3_PORT		0
	#define UART3_TX_PIN	25
	#define UART3_RX_PIN	26
#elif defined(LINCOLN60)
	#define UART3_PORT		0
	#define UART3_TX_PIN	0
	#define UART3_RX_PIN	1
#endif

//#define	UART_CFG_LOCATION	0x7fc00
typedef struct {
	uint32_t baud;
	char mode[4];
} UART_FLASH_CONFIG;

typedef struct {
	uint8_t id;
	app_type_t app_type;
	char name[6];
	LPC_USART_T *uart;
	IRQn_Type irq;
	uint16_t control_port;
	uint16_t serial_port;
	uint32_t baud;
	char mode[4];
	uint64_t rx_bytes;
	uint32_t tx_bytes;
	uint32_t rx_lost;
	uint32_t tx_lost;
	xSocket_t *serial_socks;
	xSocket_t *serctl_socks;
	char master_sock_bit;
	RINGBUFF_T rxring;
	RINGBUFF_T txring;
	uint8_t rxbuff[UART_RRB_SIZE];
	uint8_t txbuff[UART_SRB_SIZE];
} UART_CONFIG;


int set_uart_config(SERIAL_CFG *cfg, int baud, char* mode, char *error, bool nofail);
void setup_uart0(SERIAL_CFG *cfg, uint32_t baud, char *mode);
void setup_uart3(SERIAL_CFG *cfg, uint32_t baud, char *mode);
void send_break(int uart_num);
