/*
 * FreeRTOS Kernel V10.0.1
 * Copyright (C) 2017 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 * 1 tab == 4 spaces!
 */

#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

#include <stdint.h>
extern uint32_t SystemCoreClock;

/*-----------------------------------------------------------
 * Application specific definitions.
 *
 * These definitions should be adjusted for your particular hardware and
 * application requirements.
 *
 * THESE PARAMETERS ARE DESCRIBED WITHIN THE 'CONFIGURATION' SECTION OF THE
 * FreeRTOS API DOCUMENTATION AVAILABLE ON THE FreeRTOS.org WEB SITE.
 * http://www.freertos.org/a00110.html
 *
 * The bottom of this file contains some constants specific to running the UDP
 * stack in this demo.  Constants specific to FreeRTOS+UDP itself (rather than
 * the demo) are contained in FreeRTOSIPConfig.h.
 *----------------------------------------------------------*/

#define configUSE_PREEMPTION			1
#define configUSE_PORT_OPTIMISED_TASK_SELECTION 1
#define configUSE_TICKLESS_IDLE			0
#define configMAX_PRIORITIES			( 7 )
#define configCPU_CLOCK_HZ				( SystemCoreClock )
#define configTICK_RATE_HZ				100
#define configMINIMAL_STACK_SIZE		( ( unsigned short ) 300 )
#define configTOTAL_HEAP_SIZE			( ( size_t ) ( 32 * 1024 ) )
#define configMAX_TASK_NAME_LEN			( 9 )
#define configIDLE_SHOULD_YIELD			0
#define configQUEUE_REGISTRY_SIZE		10
#define configUSE_TRACE_FACILITY		1
#define configUSE_16_BIT_TICKS			0
#define configUSE_MUTEXES				1
#define configUSE_CO_ROUTINES 			0
#define configMAX_CO_ROUTINE_PRIORITIES ( 2 )
#define configUSE_COUNTING_SEMAPHORES 	1
#define configUSE_ALTERNATIVE_API 		0
#define configUSE_RECURSIVE_MUTEXES		1

/* Hook function related definitions. */
#define configUSE_TICK_HOOK				0
#define configUSE_IDLE_HOOK				0
#define configUSE_MALLOC_FAILED_HOOK	1
#define configCHECK_FOR_STACK_OVERFLOW	2

/* Software timer related definitions. */
#define configUSE_TIMERS				1
#define configTIMER_TASK_PRIORITY		( configMAX_PRIORITIES - 1 )
#define configTIMER_QUEUE_LENGTH		5
#define configTIMER_TASK_STACK_DEPTH	configMINIMAL_STACK_SIZE

/* Run time stats gathering definitions. */
//void vMainConfigureTimerForRunTimeStats( void );
//uint32_t ulMainGetRunTimeCounterValue( void );
#define configGENERATE_RUN_TIME_STATS	0
//#define portCONFIGURE_TIMER_FOR_RUN_TIME_STATS() vMainConfigureTimerForRunTimeStats()
//#define portGET_RUN_TIME_COUNTER_VALUE() ulMainGetRunTimeCounterValue()

/* Set the following definitions to 1 to include the API function, or zero
to exclude the API function. */
#define INCLUDE_vTaskPrioritySet			1
#define INCLUDE_uxTaskPriorityGet			1
#define INCLUDE_vTaskDelete					1
#define INCLUDE_vTaskCleanUpResources		0
#define INCLUDE_vTaskSuspend				1
#define INCLUDE_vTaskDelayUntil				1
#define INCLUDE_vTaskDelay					1
#define INCLUDE_uxTaskGetStackHighWaterMark	1
#define INCLUDE_xTimerGetTimerTaskHandle	0
#define INCLUDE_xTaskGetIdleTaskHandle		0
#define INCLUDE_xQueueGetMutexHolder		1

/* This demo makes use of one or more example stats formatting functions.  These
format the raw data provided by the uxTaskGetSystemState() function in to human
readable ASCII form.  See the notes in the implementation of vTaskList() within
FreeRTOS/Source/tasks.c for limitations. */
#define configUSE_STATS_FORMATTING_FUNCTIONS	1

/* Assert statement defined for debug builds. */
#ifdef DEBUG
	#define configASSERT( x ) if( ( x ) == 0 ) { taskDISABLE_INTERRUPTS(); for( ;; ); }
#endif

/* Interrupt priority configuration settings follow.
http://www.freertos.org/RTOS-Cortex-M3-M4.html */

/* Use the system definition for the number of interrupt priorities, if there
is one */
#ifdef __NVIC_PRIO_BITS
	#define configPRIO_BITS       __NVIC_PRIO_BITS
#else
	#define configPRIO_BITS       5                 /* 32 priority levels */
#endif

/* The maximum priority an interrupt that uses an interrupt safe FreeRTOS API
function can have.  Note that lower priority have numerically higher values.  */
#define configMAX_LIBRARY_INTERRUPT_PRIORITY	( 5 )

/* The minimum possible interrupt priority. */
#define configMIN_LIBRARY_INTERRUPT_PRIORITY	( 0x1f )

/* The lowest priority. */
#define configKERNEL_INTERRUPT_PRIORITY 		( configMIN_LIBRARY_INTERRUPT_PRIORITY << (8 - configPRIO_BITS) )

/* Priority 5, or 248 as only the top five bits are implemented. */
#define configMAX_SYSCALL_INTERRUPT_PRIORITY 	( configMAX_LIBRARY_INTERRUPT_PRIORITY << (8 - configPRIO_BITS) )

/* Definitions that map the FreeRTOS port interrupt handlers to their CMSIS
standard names. */
#define vPortSVCHandler SVC_Handler
#define xPortPendSVHandler PendSV_Handler
#define xPortSysTickHandler SysTick_Handler


/*
 * DEMO APPLICATION SPECIFIC DEFINITIONS FOLLOW FROM HERE
 */

/* Set to 1 to include "trace start" and "trace stop" CLI commands.  These
commands start and stop the FreeRTOS+Trace recording. */
#define configINCLUDE_TRACE_RELATED_CLI_COMMANDS 0

/* Dimensions a buffer that can be used by the FreeRTOS+CLI command
interpreter.  See the FreeRTOS+CLI documentation for more information:
http://www.FreeRTOS.org/FreeRTOS-Plus/FreeRTOS_Plus_CLI/ */
#define configCOMMAND_INT_MAX_OUTPUT_SIZE		1024

/* The priority used by the Ethernet MAC driver interrupt. */
#define configMAC_INTERRUPT_PRIORITY		( configMAX_LIBRARY_INTERRUPT_PRIORITY )

/* The LPC1769 Ethernet peripheral uses a DMA to transmit and receive packets.
The DMA uses a chain of descriptors to reference Ethernet buffers that are
waiting to be sent onto the network.  configNUM_TX_DESCRIPTORS
defines the total number of transmit descriptors.  An Ethernet buffer is
not assigned to a transmit descriptor until data is actually sent, but will
remain assigned to the descriptor until the descriptor is re-used.  It is not
necessary to have many transmit descriptors as the IP stack task will be held
in the Blocked state (so other tasks can run) until a descriptor becomes
available if it attempts to transmit when all the descriptors are in use.  See
the iptraceWAITING_FOR_TX_DMA_DESCRIPTOR() IP trace macro. */
#define configNUM_TX_DESCRIPTORS 3

/* The LPC1769 Ethernet peripheral uses a DMA to transmit and receive packets.
The DMA uses a chain of descriptors to reference Ethernet buffers, and provide
information on the state of each buffer (full/empty/error/etc.).
configNUM_RX_DESCRIPTORS defines the total number of receive
descriptors (descriptors that point to buffers into which the DMA will write
packets received from the network).  An Ethernet buffer is assigned to each
descriptor.  Having too few descriptors will impact reliability because the DMA
will have to drop packets that are received when there are no receive
descriptors free.  It is however only necessary to have a couple of free
descriptors at a time, and having more wastes the RAM used by the Ethernet
buffers that are surplus to requirements. */
#define configNUM_RX_DESCRIPTORS 4

/* The LPC1769 Ethernet driver will check periodically if the link status at
the PHY has changed (cable disconnected, speed or duplex changed).
configPHY_CHECK_TIME sets the time in milliseconds of how often that check
will be executed. */
#define configPHY_CHECK_TIME	500UL

/* When the LPC1769 Ethernet driver receives an RX interrupt it will retrieve
the packet but instead of waiting for another interrupt it can check immediately
if there are more packets to retrieve and process them to improve download
performance. However, after a while we have to yield processing time to send out
queued up packets. configRX_PRIORITY_BIAS sets the maximum amount of RX packets
that will be processed in one go. Increase number to optimize for receive
performance or lower (minimum is 1) to have a more balanced RX/TX priority.
Note: when using TCP there are still ACK packets which need to get sent so the
only reason to increase this number significantly is for streaming fast UDP data
into the LPC1769. */
#define configRX_PRIORITY_BIAS	3


/* If configINCLUDE_DEMO_DEBUG_STATS is set to one, then a few basic IP trace
macros are defined to gather some UDP stack statistics that can then be viewed
through the CLI interface.  See
http://www.FreeRTOS.org/FreeRTOS-Plus/FreeRTOS_Plus_UDP/UDP_IP_Trace.shtml*/
#define configINCLUDE_DEMO_DEBUG_STATS 1

/* To get accurage stack usage through Eclipse's debugger, this should be set.
 * Once debugging is done and you're confidant you won't overflow the stack,
 * this can safely be disabled.*/

//#define configRECORD_STACK_HIGH_ADDRESS (1)

/* Only include the trace macro definitions required by FreeRTOS+Trace if
the trace start and trace stop CLI commands are included. */
//#include "trcRecorder.h"


#endif /* FREERTOS_CONFIG_H */
