

/* Standard includes. */
#include <stdint.h>
#include <string.h>

#include "board.h"
#include "lpc_phy.h"

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* FreeRTOS+TCP includes. */
#include "FreeRTOS_IP.h"
#include "FreeRTOS_IP_Private.h"
#include "NetworkBufferManagement.h"

/* Driver includes. */
//#include "lpc17xx_emac.h"
//#include "lpc17xx_pinsel.h"

/* Demo includes. */
#include "NetworkInterface.h"

#define ENET_NUM_TX_DESC 5
#define ENET_NUM_RX_DESC 6

#define ENET_RX_DESC_BASE        (0x2007C000)

#define ENET_RX_STAT_BASE        (ENET_RX_DESC_BASE + ENET_NUM_RX_DESC * sizeof(ENET_RXDESC_T))
#define ENET_TX_DESC_BASE        (ENET_RX_STAT_BASE + ENET_NUM_RX_DESC * sizeof(ENET_RXSTAT_T))
#define ENET_TX_STAT_BASE        (ENET_TX_DESC_BASE + ENET_NUM_TX_DESC * sizeof(ENET_TXDESC_T))
#define ENET_RX_BUF_BASE         (ENET_TX_STAT_BASE + ENET_NUM_TX_DESC * sizeof(ENET_TXSTAT_T))
#define ENET_TX_BUF_BASE         (ENET_RX_BUF_BASE  + ENET_NUM_RX_DESC * ENET_ETH_MAX_FLEN)
#define ENET_RX_BUF(i)           (ENET_RX_BUF_BASE + ENET_ETH_MAX_FLEN * i)
#define ENET_TX_BUF(i)           (ENET_TX_BUF_BASE + ENET_ETH_MAX_FLEN * i)

static ENET_RXDESC_T *pRXDescs = (ENET_RXDESC_T *) ENET_RX_DESC_BASE;
static ENET_RXSTAT_T *pRXStats = (ENET_RXSTAT_T *) ENET_RX_STAT_BASE;
static ENET_TXDESC_T *pTXDescs = (ENET_TXDESC_T *) ENET_TX_DESC_BASE;
static ENET_TXSTAT_T *pTXStats = (ENET_TXSTAT_T *) ENET_TX_STAT_BASE;

/* Transmit/receive buffers and indices */
static int32_t rxConsumeIdx;
static int32_t txProduceIdx;

#define	EMAC_EVT_RX		1
#define EMAC_EVT_TX		2

static volatile uint8_t intEvents = 0;

static TaskHandle_t xRxHandlerTask = NULL;

NetworkBufferDescriptor_t *tx_descriptors[ENET_NUM_TX_DESC];
static SemaphoreHandle_t xTXDescriptorSemaphore = NULL;
static SemaphoreHandle_t xHandlerSemaphore = NULL;


static int tx_desc_to_free = 0;
static int tx_nb_to_free = 0;
static int globci = -1;


int sent = 0;
int cleared = 0;
int txdone = 0;
int txur = 0;
int txfin = 0;
int txerr = 0;
int txloops = 0;
size_t freeheap;
static int releaseCount = 0;
static int requestCount = 0;


static void prvEMACHandlerTask(void *pvParameters);


void emac_msleep(uint32_t ms)
{
	volatile uint32_t i, count = ms * 1000;

	while (--count > 0) {
		/*
		 * One for loop is 6 instructions,
		 * at 96MHz that comes to 16 for-loops per us.
		 */
		for (i = 0; i < 16; i++) {
			/* null */ ;
		}
	}
}



/* Initialize MAC descriptors for simple packet receive/transmit */
static void 
init_descriptors()
{
	int i;

	/* Setup the descriptor list to a default state */
	memset(pTXDescs, 0, ENET_NUM_TX_DESC * sizeof(ENET_TXDESC_T));
	memset(pTXStats, 0, ENET_NUM_TX_DESC * sizeof(ENET_TXSTAT_T));
	memset(pRXDescs, 0, ENET_NUM_RX_DESC * sizeof(ENET_RXDESC_T));
	memset(pRXStats, 0, ENET_NUM_RX_DESC * sizeof(ENET_RXSTAT_T));

	rxConsumeIdx = 0;
    rxConsumeIdx = 0;

	/* Build linked list, CPU is owner of descriptors */
	for (i = 0; i < ENET_NUM_RX_DESC; i++) {
		pRXDescs[i].Packet = (uint32_t) ENET_RX_BUF(i);
		pRXDescs[i].Control = ENET_RCTRL_INT | ENET_RCTRL_SIZE(ENET_ETH_MAX_FLEN);
		pRXStats[i].StatusInfo = 0;
		pRXStats[i].StatusHashCRC = 0;
	}
	for (i = 0; i < ENET_NUM_TX_DESC; i++) {
		pTXDescs[i].Packet = (uint32_t) ENET_TX_BUF(i);
		pTXDescs[i].Control = 0;
		pTXStats[i].StatusInfo = 0;
	}

	/* Setup list pointers in Ethernet controller */
	Chip_ENET_InitTxDescriptors(LPC_ETHERNET, pTXDescs, pTXStats, ENET_NUM_TX_DESC);
	Chip_ENET_InitRxDescriptors(LPC_ETHERNET, pRXDescs, pRXStats, ENET_NUM_RX_DESC);
}

/* Get Tx Buffer for the next transmission */
static void *ENET_TXBuffGet(void)
{
	uint16_t consumeIdx = Chip_ENET_GetTXConsumeIndex(LPC_ETHERNET);

	if (Chip_ENET_GetBufferStatus(LPC_ETHERNET, txProduceIdx, consumeIdx, ENET_NUM_TX_DESC) != ENET_BUFF_FULL) {
		return (void *) pTXDescs[txProduceIdx].Packet;
	}
	return NULL;
}

/* Get Tx Buffer for the next transmission */
static int ENET_TXBuffSet(void *buf)
{
	uint16_t consumeIdx = Chip_ENET_GetTXConsumeIndex(LPC_ETHERNET);

	if (Chip_ENET_GetBufferStatus(LPC_ETHERNET, txProduceIdx, consumeIdx, ENET_NUM_TX_DESC) != ENET_BUFF_FULL) {
		pTXDescs[txProduceIdx].Packet = (uint32_t)buf;
		return true;
	}
	return false;
}


/* Queue a new frame for transmission */
static void ENET_TXQueue(int32_t bytes)
{
	if (bytes > 0) {
		pTXDescs[txProduceIdx].Control = ENET_TCTRL_SIZE(bytes) | ENET_TCTRL_LAST | ENET_TCTRL_INT;
		txProduceIdx = Chip_ENET_IncTXProduceIndex(LPC_ETHERNET);
	}
}

/* Check if tranmission finished */
static bool ENET_IsTXFinish(void)
{
	uint16_t consumeIdx = Chip_ENET_GetTXConsumeIndex(LPC_ETHERNET);

	if (Chip_ENET_GetBufferStatus(LPC_ETHERNET, txProduceIdx, consumeIdx, ENET_NUM_TX_DESC) == ENET_BUFF_EMPTY) {
		return true;
	}
	return false;
}

static void *ENET_RXGet(int32_t *bytes)
{
	uint16_t produceIdx;
	void *buffer;

	produceIdx = Chip_ENET_GetRXProduceIndex(LPC_ETHERNET);
	/* This doesn't check status of the received packet */
	if (Chip_ENET_GetBufferStatus(LPC_ETHERNET, produceIdx, rxConsumeIdx, ENET_NUM_RX_DESC) != ENET_BUFF_EMPTY) {
		/* CPU owns descriptor, so a packet was received */
		buffer = (void *) pRXDescs[rxConsumeIdx].Packet;
		*bytes = (int32_t) (ENET_RINFO_SIZE(pRXStats[rxConsumeIdx].StatusInfo) - 4);/* Remove CRC */
	}
	else {
		/* Nothing received */
		*bytes = 0;
		buffer = NULL;
	}

	return buffer;
}

STATIC void ENET_RXBuffClaim(void)
{
	rxConsumeIdx = Chip_ENET_IncRXConsumeIndex(LPC_ETHERNET);
}



BaseType_t xNetworkInterfaceInitialise( void )
{
    /*
     * Perform the hardware specific network initialisation here.  Typically
     * that will involve using the Ethernet driver library to initialise the
     * Ethernet (or other network) hardware, initialise DMA descriptors, and
     * perform a PHY auto-negotiation to obtain a network link.
     *
     */

	// TODO: get from function
	const uint8_t ucMACAddress[6] = {0x00, 0x0c, 0x6a, 0x01, 0x03, 0x10};
	BaseType_t xReturn = pdPASS;

	Chip_ENET_Init(LPC_ETHERNET, true);
	Chip_ENET_SetupMII(LPC_ETHERNET, Chip_ENET_FindMIIDiv(LPC_ETHERNET, 2500000), 1);
	lpc_phy_init(true, emac_msleep, 0, 0);
	Chip_ENET_SetADDR(LPC_ETHERNET, ucMACAddress);
	init_descriptors();
	
	/* Enable RX/TX after descriptors are setup */
	Chip_ENET_TXEnable(LPC_ETHERNET);
	Chip_ENET_RXEnable(LPC_ETHERNET);
	
	Chip_ENET_EnableRXFilter(LPC_ETHERNET, ENET_RXFILTERCTRL_APE | ENET_RXFILTERCTRL_ABE);

	if (xHandlerSemaphore == NULL) {
		xHandlerSemaphore = xSemaphoreCreateBinary();
		configASSERT(xHandlerSemaphore);
	}

	xReturn = xTaskCreate(prvEMACHandlerTask, "EMAC", configMINIMAL_STACK_SIZE,
	  NULL, configMAX_PRIORITIES - 1, &xRxHandlerTask );
	configASSERT( xReturn );
	
	
	if( xTXDescriptorSemaphore == NULL ) {
		xTXDescriptorSemaphore = xSemaphoreCreateCounting(
		  (UBaseType_t) ENET_NUM_TX_DESC - 1,
		  (UBaseType_t) ENET_NUM_TX_DESC - 1);
		configASSERT( xTXDescriptorSemaphore );
	}
	

    return xReturn;
}


BaseType_t xNetworkInterfaceOutput( NetworkBufferDescriptor_t * const pxDescriptor,
                                    BaseType_t xReleaseAfterSend )
{
    /* Simple network interfaces (as opposed to more efficient zero copy network
    interfaces) just use Ethernet peripheral driver library functions to copy
    data from the FreeRTOS+TCP buffer into the peripheral driver's own buffer.
    This example assumes SendData() is a peripheral driver library function that
    takes a pointer to the start of the data to be sent and the length of the
    data to be sent as two separate parameters.  The start of the data is located
    by pxDescriptor->pucEthernetBuffer.  The length of the data is located
    by pxDescriptor->xDataLength. */

	
	uint8_t *buf;
	BaseType_t xReturn = pdFAIL;
	TickType_t xBlockTimeTicks = pdMS_TO_TICKS(100);
	TimeOut_t xTimeOut;
	bool nobuf = false;
	bool sentout = false;
	
	
	do {
		if(xTXDescriptorSemaphore == NULL) {
			break;
		}
		if( xSemaphoreTake(xTXDescriptorSemaphore, xBlockTimeTicks) != pdPASS ) {
			/* Time-out waiting for a free TX descriptor. */
			break;
		}
	
		vTaskSetTimeOutState( &xTimeOut );
		
//		while ((buf = ENET_TXBuffGet()) == NULL) {
//			if( xTaskCheckForTimeOut( &xTimeOut, &xBlockTimeTicks ) != pdFALSE ) {
//				// couldn't get free descriptor
//				nobuf = true;
//				break;
//			}
//		}
//		if (nobuf || pxDescriptor->pucEthernetBuffer == 0) {
//			xSemaphoreGive(xTXDescriptorSemaphore);
//			break;
//		}
//		
//		memcpy(buf, pxDescriptor->pucEthernetBuffer, pxDescriptor->xDataLength);
		
		while (ENET_TXBuffSet(pxDescriptor->pucEthernetBuffer) == false) {
			if( xTaskCheckForTimeOut( &xTimeOut, &xBlockTimeTicks ) != pdFALSE ) {
				// couldn't get free descriptor
				nobuf = true;
				break;
			}
		}
		if (nobuf) {
			xSemaphoreGive(xTXDescriptorSemaphore);
			break;
		}
		
		if (xReleaseAfterSend != pdFALSE) {
			tx_descriptors[Chip_ENET_GetTXProduceIndex(LPC_ETHERNET)] = \
			  pxDescriptor;
		}

		ENET_TXQueue(pxDescriptor->xDataLength);
		sent++;
		sentout = true;
		//xSemaphoreGive(xTXDescriptorSemaphore);
		

		/* Call the standard trace macro to log the send event. */
		iptraceNETWORK_INTERFACE_TRANSMIT();
	} while (0);
	
	if (!sentout && xReleaseAfterSend != pdFALSE) {
			/* we had some problem sending paket out, make sure we clear buffer if required */
		  vReleaseNetworkBufferAndDescriptor( pxDescriptor );
	}

//    if( xReleaseAfterSend != pdFALSE )
//    {
//        /* It is assumed SendData() copies the data out of the FreeRTOS+TCP Ethernet
//        buffer.  The Ethernet buffer is therefore no longer needed, and must be
//        freed for re-use. */
//        vReleaseNetworkBufferAndDescriptor( pxDescriptor );
//    }

    return xReturn;
}

void ENET_IRQHandler( void )
{
	uint32_t intStatus = 1;
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	
	while((intStatus = Chip_ENET_GetIntStatus(LPC_ETHERNET)) != 0 )
	{
		/* Clear the interrupt. */
		Chip_ENET_ClearIntStatus(LPC_ETHERNET, intStatus);

		if (intStatus & ENET_INT_RXDONE) {
			intEvents |= EMAC_EVT_RX;
			vTaskNotifyGiveFromISR(xRxHandlerTask, &xHigherPriorityTaskWoken);
			//xSemaphoreGiveFromISR(xHandlerSemaphore, &xHigherPriorityTaskWoken);
		}
		if (intStatus & ENET_INT_TXDONE) {
			txdone++;
//			if (txdone - cleared >= 4) {
//				txdone = txdone;
//			}
			globci = Chip_ENET_GetTXConsumeIndex(LPC_ETHERNET);
			intEvents |= EMAC_EVT_TX;
			vTaskNotifyGiveFromISR(xRxHandlerTask, &xHigherPriorityTaskWoken);
			//xSemaphoreGiveFromISR(xHandlerSemaphore, &xHigherPriorityTaskWoken);
		}
		if (intStatus & ENET_INT_RXOVERRUN) {
			Chip_ENET_ResetRXLogic(LPC_ETHERNET);
			rxConsumeIdx = 0;
		}
		/* Clear fatal error conditions.  NOTE:  The driver does not clear all
		errors, only those actually experienced.  For future reference, range
		errors are not actually errors so can be ignored. */
		if (intStatus & ENET_INT_TXUNDERRUN) {
			txur++;
			Chip_ENET_ResetTXLogic(LPC_ETHERNET);
		}
/*		
		if (intStatus & ENET_INT_TXERROR) {
			txerr++;
		}
		if (intStatus & ENET_INT_TXFINISHED) {
			txfin++;
			intEvents |= EMAC_EVT_TX;
			vTaskNotifyGiveFromISR(xRxHandlerTask, &xHigherPriorityTaskWoken);
		}
*/
	}

	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}

static void prvEMACHandlerTask( void *pvParameters )
{
	const TickType_t xBlockTime = pdMS_TO_TICKS( 500ul );
	const TickType_t xDescriptorWaitTime = pdMS_TO_TICKS( 250 );
	uint8_t *buf;
	int32_t rx_bytes;
	NetworkBufferDescriptor_t *pxDescriptor;
	IPStackEvent_t xRxEvent = { eNetworkRxEvent, NULL };
  int lcleared;
	
	/* have to turn on interrupts here to make sure we are ready
	 *to receive notifications from ulTaskNotify... */
	Chip_ENET_EnableInt(LPC_ETHERNET, ~0);
	NVIC_SetPriority(ETHERNET_IRQn, configMAC_INTERRUPT_PRIORITY);
	NVIC_EnableIRQ(ETHERNET_IRQn);
	
	for( ;; ) {
		freeheap = xPortGetFreeHeapSize();

		ulTaskNotifyTake(pdTRUE,  xBlockTime);
		//xSemaphoreTake(xHandlerSemaphore, xBlockTime);
		if (intEvents & EMAC_EVT_RX) {
			int rxcount = 0;
			intEvents &= ~EMAC_EVT_RX;
			
			while ((buf = ENET_RXGet(&rx_bytes)) != NULL) {
				if (!rx_bytes) {
					ENET_RXBuffClaim();
					continue;
				}
				pxDescriptor = pxGetNetworkBufferWithDescriptor(rx_bytes,
				  xDescriptorWaitTime );
				if (!pxDescriptor) {
					ENET_RXBuffClaim();
					continue;
				}
				//pxDescriptor->pucEthernetBuffer = buf;
				memcpy(pxDescriptor->pucEthernetBuffer, buf, rx_bytes);
				pxDescriptor->xDataLength = rx_bytes;
				xRxEvent.pvData = (void *)pxDescriptor;
				if( xSendEventStructToIPTask(&xRxEvent, (TickType_t) 0) == pdFAIL) {
					vReleaseNetworkBufferAndDescriptor(pxDescriptor);
					iptraceETHERNET_RX_EVENT_LOST();
				}
				ENET_RXBuffClaim();
				if (rxcount++ > 5) break;
			}
		}
		if (intEvents & EMAC_EVT_TX) {
			int ci;
			lcleared = 0;
			
			

			txloops++;
			
			if (txdone - cleared >= 4) {
				txdone = txdone;
			}
			while (1) {
				ci = Chip_ENET_GetTXConsumeIndex(LPC_ETHERNET);
				/*
				 * desc to free must be smaller than ConsumeIdx 
				 * however, if desc to free is last in list, every ConsumeIdx
 				 * except the last one is larger than the desc to free
				 */
				//if (tx_desc_to_free < ci || (tx_desc_to_free == (ENET_NUM_TX_DESC - 1) && ci != tx_desc_to_free)) {
				if (tx_nb_to_free != ci) { 
					if (tx_descriptors[tx_nb_to_free] != NULL) {
							vReleaseNetworkBufferAndDescriptor(tx_descriptors[tx_nb_to_free]);
							tx_descriptors[tx_nb_to_free] = NULL;
					}
					xSemaphoreGive(xTXDescriptorSemaphore);
					cleared++;
					lcleared++;
					//if(++tx_desc_to_free >= ENET_NUM_TX_DESC) tx_desc_to_free = 0;
					if (++tx_nb_to_free >= ENET_NUM_TX_DESC) tx_nb_to_free = 0;
				} else {
					if (lcleared == 0) {
						  lcleared = lcleared;
					}
					if (lcleared == 1) {
						  lcleared = lcleared;
					}
					if (lcleared == 2) {
						  lcleared = lcleared;
					}
					if (lcleared == 3) {
						  lcleared = lcleared;
					}
					if (lcleared > 3) {
						  lcleared = lcleared;
					}
					
					break;
				}
			}
			intEvents &= ~EMAC_EVT_TX;
		}
	}
}


void countRelease(void *x)
{
	releaseCount++;
}

void countRequest(void *x)
{
	requestCount++;
}

void traceMalloc(void *ptr, int size)
{
	static void* malloced[256];
	static int smalloced[256];
	
	static int idx = 0;
	smalloced[idx] = size;
	malloced[idx++] = ptr;
}

void traceFree(void *ptr, int size)
{
	static void* freed[256];
	static int sfreed[256];
	
	static int idx = 0;
	sfreed[idx] = size;
	freed[idx++] = ptr;
}




