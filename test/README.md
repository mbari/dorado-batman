To test TCP/UDP UART comms:

Use the UDP spammer to generate traffic to one of the battery ports (UART0 in this case):

`$ python udp_spammer.py --ip 134.89.32.133 --port 20001`

Use a loopback connector between UART0 and UART3 on the battery:
```
UART0_TX_________________________  __________UART3_TX
								 \/
UART0_RX_________________________/\__________UART3_RX

UART0_GND____________________________________UART3_GND
```

Check for incoming TCP packets hitting the port corresponding to UART3:

`$ telnet 134.89.32.133 10002`

Check for UDP packets containing the identical payload for the UDP port on UART3:

`$ nc -u -l -p 20002`