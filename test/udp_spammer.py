#!/bin/python3

import socket
import argparse
import time
import random
import string


def open_udp_socket(host='127.0.0.1', port=20001):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print("host: {}".format(host))
    s.bind((socket.gethostname(), port))
    s.settimeout(0.050) #50 ms timeout
    return s

def random_str(length=16):
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(length))



if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='udp_spammer',
                                    usage='%(prog)s --ip <IP> --port <PORT>',
                                    description='Send random strings via UDP to <IP>:<PORT>. See README.md for additional information.')
    parser.add_argument('--ip', type=str, dest='ip', required=True)
    parser.add_argument('--port', type=int, dest='port', required=True)

    args = parser.parse_args()

    # Open UDP port
    sock = open_udp_socket(args.ip, args.port)

    # for i in range(55):
    #     cmd = '@{}PF\n\r'.format(str(i).zfill(2))
    #     print('Sending ' + cmd)
    #     sock.sendto(cmd.encode('utf-8'), (args.ip, args.port))
    #     time.sleep(0.25)
    #     data, address = sock.recvfrom(32768)
    #     print(data.decode('utf-8'))
    while True:
        # Do spamming here
        rand_str = random_str(891)+"\n\r"
        sock.sendto(rand_str.encode('utf-8'), (args.ip, args.port))
        time.sleep(0.5)        
    
    exit(0)
